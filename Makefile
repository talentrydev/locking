up:
	docker-compose -f infrastructure/dev/docker-compose.yml -p locking up -d
down:
	docker-compose -f infrastructure/dev/docker-compose.yml -p locking down
test:
	docker-compose -f infrastructure/dev/docker-compose.yml -p locking exec php-7.4-cli vendor/bin/phpunit
	docker-compose -f infrastructure/dev/docker-compose.yml -p locking exec php-8.0-cli vendor/bin/phpunit
cs:
	docker-compose -f infrastructure/dev/docker-compose.yml -p locking exec php-8.0-cli vendor/bin/phpcs --standard=PSR12 src
cs-fix:
	docker-compose -f infrastructure/dev/docker-compose.yml -p locking exec php-8.0-cli vendor/bin/phpcbf --standard=PSR12 src
deps-7.4:
	docker-compose -f infrastructure/dev/docker-compose.yml -p locking exec php-7.4-cli composer install
deps-8.0:
	docker-compose -f infrastructure/dev/docker-compose.yml -p locking exec php-8.0-cli composer install
