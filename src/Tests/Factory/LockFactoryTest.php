<?php

declare(strict_types=1);

namespace Talentry\Locking\Tests\Factory;

use PHPUnit\Framework\TestCase;
use Talentry\Locking\Exception\LockAcquireException;
use Talentry\Locking\Factory\LockFactory;
use Doctrine\DBAL\Connection;
use Talentry\Locking\Lock;
use Talentry\Locking\Monitoring\Metric\LockAcquireFailed;
use Talentry\Locking\MySql\MySqlLock;
use Talentry\Monitoring\Domain\Monitor\Monitor;
use Doctrine\DBAL\Driver\Statement;
use Exception;

class LockFactoryTest extends TestCase
{
    private LockFactory $lockFactory;
    private Connection $connection;
    private Monitor $monitor;
    private Lock $lock;

    public function testLockType(): void
    {
        $this->whenLockIsGenerated();
        $this->expectMysqlLock();
    }

    public function testLockConnection(): void
    {
        $this->whenLockIsGenerated();
        $this->expectProvidedConnectionToBeUsedToAcquireLock();
    }

    public function testLockMonitor(): void
    {
        $this->whenLockIsGenerated();
        $this->expectProvidedMonitorWillBeUsedToPushMetrics();
    }

    private function whenLockIsGenerated(): void
    {
        $this->lock = $this->lockFactory->generate();
    }

    private function expectMysqlLock(): void
    {
        self::assertInstanceOf(MySqlLock::class, $this->lock);
    }

    private function expectProvidedConnectionToBeUsedToAcquireLock(): void
    {
        $statement = $this->createMock(Statement::class);
        $this->connection->expects(self::once())->method('prepare')->willReturn($statement);
        $this->lock->acquire('foo');
    }

    private function expectProvidedMonitorWillBeUsedToPushMetrics(): void
    {
        $this->connection->expects(self::once())->method('prepare')->willThrowException(new Exception());
        $this->monitor->expects(self::once())->method('push')->with($this->isInstanceOf(LockAcquireFailed::class));
        try {
            $this->lock->acquire('foo');
        } catch (LockAcquireException $e) {
        }
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->connection = $this->createMock(Connection::class);
        $this->monitor = $this->createMock(Monitor::class);
        $this->lockFactory = new LockFactory($this->connection, $this->monitor);
    }
}
