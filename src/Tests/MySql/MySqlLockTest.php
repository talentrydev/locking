<?php

declare(strict_types=1);

namespace Talentry\Locking\Tests\MySql;

use Doctrine\DBAL\DriverManager;
use Exception;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Exception\DeadlockException;
use PHPUnit\Framework\TestCase;
use Talentry\Locking\Exception\LockAcquireException;
use Talentry\Locking\Exception\LockReleaseException;
use Talentry\Locking\MySql\MySqlLock;
use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Monitor\Monitor;
use Talentry\Monitoring\Domain\Tags\HasTags;

class MySqlLockTest extends TestCase
{
    private Monitor $monitor;
    private Connection $connection;
    private string $lockName = 'test';

    protected function setUp(): void
    {
        parent::setUp();

        $this->monitor = $this->createMonitorMock();
        $this->connection = DriverManager::getConnection(['url' => getenv('DB_URL')]);
    }

    public function testReentrantLock(): void
    {
        $this->expectThatLockCanBeAcquired();
        $this->expectThatLockCanBeAcquired();
        $this->expectThatLockCanBeReleased();
        $this->expectThatLockCanBeReleased();

        //no more locks left to release
        $this->expectThatLockCannotBeReleased();
    }

    public function testDeadlockExceptionWhileAcquiringLock(): void
    {
        $this->givenThatDeadlockExceptionWillOccur();
        $this->expectThatLockCannotBeAcquired();
    }

    public function testConnectionExceptionWhileAcquiringLock(): void
    {
        $this->givenThatConnectionExceptionWillOccur();
        $this->expectExceptionWhileAcquiringLock();
    }

    public function testConnectionExceptionWhileReleasingLock(): void
    {
        $this->givenThatConnectionExceptionWillOccur();
        $this->expectExceptionWhileReleasingLock();
    }

    private function createMySqlLock(Connection $connection): MySqlLock
    {
        return new MySqlLock($connection, $this->monitor);
    }

    private function givenThatConnectionExceptionWillOccur(): void
    {
        $this->connection = $this->createMock(Connection::class);
        $this->connection->method('prepare')->willThrowException(new ConnectionException());
    }

    private function givenThatDeadlockExceptionWillOccur(): void
    {
        $this->connection = $this->createMock(Connection::class);
        $this->connection->method('prepare')->willThrowException($this->createMock(DeadlockException::class));
    }

    private function expectThatMetricWasPushed(string $metricName): void
    {
        foreach ($this->monitor->getPushedMetrics() as $pushedMetric) {
            if ($pushedMetric->getName() === $metricName) {
                self::assertInstanceOf(HasTags::class, $pushedMetric);
                /** @var HasTags $pushedMetric */
                $tags = $pushedMetric->getTags();
                self::assertArrayHasKey('lockName', $tags);
                self::assertSame($this->lockName, $tags['lockName']);

                return;
            }
        }

        self::fail('Expected metric was not pushed');
    }

    private function expectThatNoMetricsWerePushed(): void
    {
        self::assertEmpty($this->monitor->getPushedMetrics());
    }

    private function expectThatLockCanBeAcquired(): void
    {
        self::assertTrue($this->createMySqlLock($this->connection)->acquire($this->lockName));
        $this->expectThatNoMetricsWerePushed();
    }

    private function expectThatLockCannotBeAcquired(): void
    {
        self::assertFalse($this->createMySqlLock($this->connection)->acquire($this->lockName));
        $this->expectThatMetricWasPushed('lock.acquire-failed');
    }

    private function expectThatLockCanBeReleased(): void
    {
        self::assertTrue($this->createMySqlLock($this->connection)->release($this->lockName));
        $this->expectThatNoMetricsWerePushed();
    }

    private function expectThatLockCannotBeReleased(): void
    {
        self::assertFalse($this->createMySqlLock($this->connection)->release($this->lockName));
        $this->expectThatMetricWasPushed('lock.release-conflict');
    }

    private function expectExceptionWhileAcquiringLock(): void
    {
        $this->expectExceptionInCallable(
            LockAcquireException::class,
            fn () => $this->createMySqlLock($this->connection)->acquire($this->lockName)
        );
        $this->expectThatMetricWasPushed('lock.acquire-failed');
    }

    private function expectExceptionWhileReleasingLock(): void
    {
        $this->expectExceptionInCallable(
            LockReleaseException::class,
            fn () => $this->createMySqlLock($this->connection)->release($this->lockName)
        );
        $this->expectThatMetricWasPushed('lock.release-failed');
    }

    private function expectExceptionInCallable(string $exceptionClass, callable $callable): void
    {
        $caught = false;
        try {
            $callable();
        } catch (Exception $e) {
            $caught = $e instanceof $exceptionClass;
        }

        self::assertTrue($caught, sprintf('%s expected, but not thrown', $exceptionClass));
    }

    private function createMonitorMock(): Monitor
    {
        return new class implements Monitor {
            private array $metrics = [];

            public function push(Metric $metric): void
            {
                $this->metrics[] = $metric;
            }

            public function supports(Metric $metric): bool
            {
                return true;
            }

            public function getPushedMetrics(): array
            {
                return $this->metrics;
            }
        };
    }
}
