<?php

declare(strict_types=1);

namespace Talentry\Locking\Tests\Mocks;

use Talentry\Locking\Lock;

class MockLock implements Lock
{
    /**
     * @var array<string,bool>
     */
    private array $locks = [];

    public function acquire(string $name, int $acquireTimeout = 0): bool
    {
        if (isset($this->locks[$name])) {
            return false;
        }

        $this->locks[$name] = true;

        return true;
    }

    public function release(string $name): bool
    {
        if (!isset($this->locks[$name])) {
            return false;
        }

        unset($this->locks[$name]);

        return true;
    }
}
