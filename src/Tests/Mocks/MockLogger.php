<?php

declare(strict_types=1);

namespace Talentry\Locking\Tests\Mocks;

use Psr\Log\AbstractLogger;

class MockLogger extends AbstractLogger
{
    private string $loggedMessage;

    public function log($level, $message, array $context = [])
    {
        $this->loggedMessage = $message;
    }

    public function getLoggedMessage(): string
    {
        return $this->loggedMessage;
    }
}
