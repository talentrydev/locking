<?php

declare(strict_types=1);

namespace Talentry\Locking\MySql;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception\DeadlockException;
use LogicException;
use Talentry\Locking\Exception\LockAcquireException;
use Talentry\Locking\Exception\LockReleaseException;
use Talentry\Locking\Lock;
use Talentry\Locking\Monitoring\Metric\LockAcquireFailed;
use Talentry\Locking\Monitoring\Metric\LockReleaseConflict;
use Talentry\Locking\Monitoring\Metric\LockReleaseFailed;
use Talentry\Locking\Monitoring\Metric\LockUnavailable;
use Talentry\Monitoring\Domain\Monitor\Monitor;
use Exception;

class MySqlLock implements Lock
{
    private const DEFAULT_TIMEOUT = 0;

    private Connection $dbConnection;
    private Monitor $monitor;

    public function __construct(Connection $dbConnection, Monitor $monitor)
    {
        $this->dbConnection = $dbConnection;
        $this->monitor = $monitor;
    }

    public function acquire(string $name, int $acquireTimeout = self::DEFAULT_TIMEOUT): bool
    {
        if ($acquireTimeout < 0) {
            throw new LogicException('Lock acquire timeout must be a non-negative integer.');
        }

        $acquireTimeout = (int) round($acquireTimeout / 1000);

        try {
            $statement = $this->dbConnection->prepare('SELECT GET_LOCK(:name, :timeout)');
            $statement->bindValue('name', $name);
            $statement->bindValue('timeout', $acquireTimeout);
            $statement->execute();
            $result = $statement->fetchColumn();
            $acquired = $result === '1';
            if (!$acquired) {
                $this->monitor->push(new LockUnavailable($name));
            }

            return $acquired;
        } catch (DeadlockException $e) {
            $this->monitor->push(new LockAcquireFailed($name));
            return false;
        } catch (Exception $e) {
            $this->monitor->push(new LockAcquireFailed($name));
            throw new LockAcquireException($e);
        }
    }

    public function release(string $name): bool
    {
        try {
            $statement = $this->dbConnection->prepare('SELECT RELEASE_LOCK(:name)');
            $statement->bindValue('name', $name);
            $statement->execute();
            $result = $statement->fetchColumn();
            $released = $result === '1';

            if (!$released) {
                $this->monitor->push(new LockReleaseConflict($name));
            }

            return $released;
        } catch (Exception $e) {
            $this->monitor->push(new LockReleaseFailed($name));
            throw new LockReleaseException($e);
        }
    }
}
