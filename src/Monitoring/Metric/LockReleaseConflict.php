<?php

declare(strict_types=1);

namespace Talentry\Locking\Monitoring\Metric;

class LockReleaseConflict extends BaseLockMetric
{
    public function getName(): string
    {
        return 'lock.release-conflict';
    }
}
