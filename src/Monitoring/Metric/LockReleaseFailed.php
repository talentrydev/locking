<?php

declare(strict_types=1);

namespace Talentry\Locking\Monitoring\Metric;

class LockReleaseFailed extends BaseLockMetric
{
    public function getName(): string
    {
        return 'lock.release-failed';
    }
}
