<?php

declare(strict_types=1);

namespace Talentry\Locking\Monitoring\Metric;

use Talentry\Monitoring\Domain\Metric\Model\IncrementMetric;
use Talentry\Monitoring\Domain\Tags\HasTags;

abstract class BaseLockMetric implements IncrementMetric, HasTags
{
    private string $lockName;

    public function __construct(string $lockName)
    {
        $this->lockName = $lockName;
    }

    public function getTags(): array
    {
        return [
            'lockName' => $this->lockName
        ];
    }
}
