<?php

declare(strict_types=1);

namespace Talentry\Locking\Monitoring\Metric;

class LockUnavailable extends BaseLockMetric
{
    public function getName(): string
    {
        return 'lock.unavailable';
    }
}
