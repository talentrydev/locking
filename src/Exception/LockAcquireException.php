<?php

declare(strict_types=1);

namespace Talentry\Locking\Exception;

class LockAcquireException extends LockException
{
}
