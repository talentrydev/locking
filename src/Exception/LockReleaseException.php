<?php

declare(strict_types=1);

namespace Talentry\Locking\Exception;

class LockReleaseException extends LockException
{
}
