<?php

declare(strict_types=1);

namespace Talentry\Locking\Exception;

use Exception;
use Throwable;

class LockException extends Exception
{
    public function __construct(Throwable $cause)
    {
        parent::__construct('', 0, $cause);
    }
}
