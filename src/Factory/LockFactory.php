<?php

declare(strict_types=1);

namespace Talentry\Locking\Factory;

use Doctrine\DBAL\Connection;
use Talentry\Locking\Lock;
use Talentry\Locking\MySql\MySqlLock;
use Talentry\Monitoring\Domain\Monitor\Monitor;
use Talentry\Monitoring\Domain\Monitor\VoidMonitor;

class LockFactory
{
    private Connection $connection;
    private Monitor $monitor;

    public function __construct(Connection $connection, ?Monitor $monitor = null)
    {
        $this->connection = $connection;
        $this->monitor = $monitor ?? new VoidMonitor();
    }

    public function generate(): Lock
    {
        return new MySqlLock($this->connection, $this->monitor);
    }
}
