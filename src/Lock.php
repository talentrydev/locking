<?php

declare(strict_types=1);

namespace Talentry\Locking;

use Talentry\Locking\Exception\LockAcquireException;
use Talentry\Locking\Exception\LockReleaseException;

interface Lock
{
    /**
     * Acquires a mutex lock identified by the provided name.
     * Only one process at a time may hold the lock with any given name.
     * If lock is successfully acquired, method returns true, otherwise returns false.
     *
     * Timeout in milliseconds. Zero means no timeout.
     *
     * @throws LockAcquireException
     */
    public function acquire(string $name, int $acquireTimeout = 0): bool;

    /**
     * Releases the lock identified by the provided name.
     * If the client doesn't hold the lock, calling this method will have no effect.
     * You cannot release a lock acquired by another client.
     * If lock is successfully released, method returns true, otherwise returns false.
     *
     * @throws LockReleaseException
     */
    public function release(string $name): bool;
}
