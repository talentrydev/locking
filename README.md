# Locking module

This module exposes a simple interface for acquiring a mutex lock.
Currently, locking is implemented using MySQL functions (GET_LOCK and RELEASE_LOCK), but clients should not rely on this,
as this is an implementation detail that is subject to change without notice.

# How to use

- Create a Lock instance using factory:
  ```
  $connection = \Doctrine\DBAL\DriverManager::getConnection(['url' => $dbUrl]); 
  $factory = new \Talentry\Locking\Factory\LockFactory($connection);
  $lock = $factory->generate();
  ```
- Call `Lock::acquire`, providing the lock name.
- If the method returns `true`, you have acquired the lock, otherwise you failed to acquire it (possibly because another client already holds the lock).
- Do some useful work.
- Call `Lock::release` (again providing the lock name), to release the lock and thus make it available to other clients.
